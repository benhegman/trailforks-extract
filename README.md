# Trailforks Extract

## Finding GeoData

1. Go to Trailforks and zoom to desired area.
2. Open Chrome developer tools by going to the three dots in the top left hand corner of the browser -> More Tools -> Developer Tools.
3. Select the "Network" Tab
4. With developer tools open, refresh the page.
5. There should be a file with "format=geojson" in the list of network files. Mine started with this: https://www.trailforks.com/rms/?rmsP=j2&mod=trailforks&op=map&format=geojson&z=11.9&layers=markers...
6. Copy that link to be used in the script

Use the package manager pip to install requirements.

1. Open PyCharm and open desired working directory. This should be where you want the output to save and where requirements.txt is located
2. Make sure a Python 3 interpreter is active.
3. Go to the Terminal tab at the bottom of PyCharm and run the following code:

```bash
pip install -r requirements.txt
```

## Run the Script

1. Replace line 7 of the script with the copied link from earlier
2. Run the full script - this will give a geojson of all the points polygons and lines in the area with all attributes
3. Use an external site like [this](https://mygeodata.cloud/converter/geojson-to-shp) to convert from geojson to Shapefile
