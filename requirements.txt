certifi==2020.6.20
chardet==3.0.4
geojson==2.5.0
idna==2.10
polyline==1.4.0
requests==2.24.0
six==1.15.0
urllib3==1.25.9
