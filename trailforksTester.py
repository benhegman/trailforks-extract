import polyline
import requests
import json
from geojson import dump

# Replace following line with you link
features = requests.get('https://www.trailforks.com/rms/?rmsP=j2&mod=trailforks&op=map&format=geojson&z=10.8&layers=markers,tracks,polygons&markertypes=poi,directory,region&region=city_3255&bboxa=-123.18059339849674,49.256105799596526,-122.79410660150455,49.39747272581451&display=difficulty&activitytype=1&condition_time=0&trailids=&rid=3255&season=0&ebike=0&filters=bikepacking::0&hideunsanctioned=0')
featuresJson = json.loads(features.text)


for feature in featuresJson['features']:
    if "encodedpath" in feature['geometry'].keys():
        coords = polyline.decode(feature['geometry']['encodedpath'])
        for coord in coords:
            coords[coords.index(coord)] = (coord[1], coord[0])
        feature['geometry']["coordinates"] = coords

with open('trailforks.geojson', 'w') as f:
   dump(featuresJson, f)



